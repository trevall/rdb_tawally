﻿//
// FILE : POSForm.cs
// PROJECT : PROG2110 - Assignment #4
// PROGRAMMER : Trevor Allain
// FIRST VERSION : 2018-12-01
// DESCRIPTION : The source code for the logic of the POSForm. This program will act like a point of sale application
//               where users can purchase vehicles from a number of stores. Also, stores can purchase vehicles as well as
//               allow for the trade in of cars.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Diagnostics;//debug console

namespace TAWally
{
    public partial class POSForm : Form
    {
        //connection string
        private string myConnection = ConfigurationManager.ConnectionStrings["TAWallyConnectionString"].ConnectionString;

        private string dealership { get; set; }
        private string vehicle { get; set; }
        private string make { get; set; }
        private string model { get; set; }
        private string VIN { get; set; }
        private string totalKM { get; set; }
        private string year { get; set; }
        private string colour { get; set; }
        private int wPrice { get; set; }
        private double sPrice { get; set; }

        private string inStock { get; set; }
        private int totalCustomers { get; set; }
        private int customerID { get; set; }
        private int orderID { get; set; }
        private int dealerID { get; set; }
        private int orderStatus { get; set; }
        private int tradeIn { get; set; }

        private string firstName { get; set; }
        private string lastName { get; set; }
        private string phoneNumber { get; set; }

        //constants for order status
        private const int STATUS_PAID = 1;
        private const int STATUS_CANCEL = 2;
        private const int STATUS_RFND = 3;
        private const int STATUS_HOLD = 4;

        //constants for dealerships
        private const int SPORTS_WORLD = 1;
        private const int GUELPH_AUTO = 2;
        private const int WATERLOO = 3;

        public POSForm()
        {
            InitializeComponent();
            using (var myConn = new MySqlConnection(myConnection))
            {
                //set up the sql commands and adapters
                var myCommand = new MySqlCommand(@"SELECT DealerName FROM Dealership;", myConn);
                var myAdapter = new MySqlDataAdapter
                {
                    SelectCommand = myCommand
                };

                //get a data set to store retrieved info
                DataSet newDS = new DataSet();

                //fill the adapter
                myAdapter.Fill(newDS, "TAWally.Dealership");

                //get each row from the query result and add it to the dealer list
                foreach (DataRow dRow in newDS.Tables["TAWally.Dealership"].Rows)
                {
                    var testString = String.Format("{0}", dRow["DealerName"]);
                    Debug.WriteLine(testString);
                    DealerList.Items.Add(testString);
                }

                FillCustomerBox();
                RefreshOrderLine();
            }

            //fill status combo box
            CustomersCombo.Visible = false;
            StatusCombo.Items.Add("PAID");
            StatusCombo.Items.Add("CANCEL");
            StatusCombo.Items.Add("REFUND");
            StatusCombo.Items.Add("HOLD");

            DealerPurchaseButton.Enabled = false;

        }


        /// <summary>
        /// This function handles the events for the dealer list drop down menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DealerList_Selection(object sender, EventArgs e)
        {
            //check to see if a dealer has been selected
            if (DealerList.SelectedItem != null)
            {
                string dealerQuery;
                MySqlCommand getVehicleInfo;
                MySqlDataAdapter myAdapter;
                listBox1.Items.Clear();

                //assign ambiguous indexes from the dealer list to constants (location)
                if (DealerList.SelectedIndex == 0)
                {
                    dealerID = SPORTS_WORLD;
                }
                else if (DealerList.SelectedIndex == 1)
                {
                    dealerID = GUELPH_AUTO;
                }
                else if (DealerList.SelectedIndex == 2)
                {
                    dealerID = WATERLOO;
                }

                //set up sql query
                using (var myConn = new MySqlConnection(myConnection))
                {

                    //query for vehicle info
                    dealerQuery = string.Format("SELECT M.Make, Mo.Model, V.Year, V.TotalKM, V.WholesalePrice, V.InStock, V.Colour, V.VehicleID FROM Vehicle V, Make M, Model Mo WHERE V.MakeID = M.MakeID AND V.ModelID = Mo.ModelID AND V.DealerShipID = {0};", dealerID);
                    getVehicleInfo = new MySqlCommand(dealerQuery, myConn);

                    myAdapter = new MySqlDataAdapter
                    {
                        SelectCommand = getVehicleInfo
                    };

                    //assign query result to data set
                    DataSet newDS = new DataSet();
                    myAdapter.Fill(newDS, "TAWally.Vehicle");
                    var header = String.Format("{0, -15} {1, -10} {2, -5} {3, -8} {4, -6} {5, -10} {6, -10} {7, -12}", "Make", "Model", "Year", "Total KM", "Price", "In Stock", "Colour", "VIN");

                    listBox1.Items.Add(header);
                    listBox1.Items.Add("");
                    listBox1.Refresh();

                    //add each row from the data set to the vehicle table
                    foreach (DataRow dRow in newDS.Tables["TAWally.Vehicle"].Rows)
                    {
                        var testString = String.Format("{0, -15} {1, -10} {2, -5} {3, -8} {4, -6} {5, -10} {6, -10} {7, -12}", dRow["Make"], dRow["Model"], dRow["Year"], dRow["TotalKM"], dRow["WholesalePrice"], dRow["InStock"], dRow["Colour"], dRow["VehicleID"]);
                        Debug.WriteLine(testString);
                        listBox1.Items.Add(testString);
                    }

                }

            }
        }


        /// <summary>
        /// This function handles the click events for the list box that contains the vehicles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void POSForm_Click(object sender, EventArgs e)
        {
            //check to see if a vehicle is selected and valid
            if (listBox1.SelectedItem != null && listBox1.SelectedIndex != 0 && listBox1.SelectedIndex != 1)
            {
                //assign vehicle properties
                string listSelect = listBox1.SelectedItem.ToString();
                string[] vehicleProps = Regex.Matches(listSelect, "\\w+").OfType<Match>().Select(x => x.Value).ToArray();
                make = vehicleProps[0];
                model = vehicleProps[1];
                year = vehicleProps[2];
                totalKM = vehicleProps[3];
                wPrice = Convert.ToInt32(vehicleProps[4]);
                inStock = vehicleProps[5];
                colour = vehicleProps[6];
                VIN = vehicleProps[7];
                MakeBox.Text = make;
                ModelBox.Text = model;
                YearBox.Text = year;
                PriceBox.Text = wPrice.ToString();
                KMBox.Text = totalKM.ToString();
                StockBox.Text = inStock;
                ColourBox.Text = colour;
                VINBox.Text = VIN;
                
                if (inStock == "No")
                {
                    MessageBox.Show("Current vehicle is not in stock! Please select an in stock vehicle!");
                    PurchaseButton.Enabled = false;
                }
                else
                {
                    PurchaseButton.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show("Please select a car!");
            }

        }

        /// <summary>
        /// This function handles the events for any clicks on th purchase button. This function
        /// also allows for the insertion and selection of various fields from the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Purchase_Click(object sender, EventArgs e)
        {

            try
            {
                //check to see if a dealer has been selected when button is pressed
                if (DealerList.SelectedItem != null)
                {
                    //check to see if a vehicle is selected
                    if (listBox1.SelectedItem != null)
                    {
                        //check to see if an existing customer option is chosen
                        if (RadioExist.Checked == true)
                        {
                            //existing customer 
                            if (CustomersCombo.SelectedItem != null)
                            {
                                //assign customer properties from the customer list
                                string listSelect = CustomersCombo.SelectedItem.ToString();
                                string[] customerProps = Regex.Matches(listSelect, "\\w+").OfType<Match>().Select(x => x.Value).ToArray();

                                firstName = customerProps[0];
                                lastName = customerProps[1];
                                phoneNumber = customerProps[2] + "-" + customerProps[3] + "-" + customerProps[4];

                                //add order and order line if a valid status is chosen
                                if (StatusCombo.SelectedItem != null)
                                {
                                    //add order 
                                    AddOrder(firstName, lastName, phoneNumber);
                                }
                                else
                                {
                                    MessageBox.Show("Please select and order status!");
                                }

                                if (TradeCheck.Checked == true)
                                {
                                    tradeIn = Convert.ToInt32(PriceBox.Text);
                                }

                                Purchase();

                            }
                            else
                            {
                                MessageBox.Show("Please select an existing customer!");
                            }
                        }
                        else if (RadioNew.Checked == true)
                        {
                            firstName = FirstBox.Text;
                            lastName = LastBox.Text;
                            phoneNumber = PhoneBox.Text;

                            //check to see if valid status is chosen for new customer
                            if (StatusCombo.SelectedItem != null)
                            {

                                //add customer and refresh the customer box
                                AddCustomer(firstName, lastName, phoneNumber);
                                FillCustomerBox();

                                //add customer order
                                AddOrder(firstName, lastName, phoneNumber);


                                //update stock values if held or paid
                                if (orderStatus == STATUS_HOLD)
                                {
                                    UpdateStock("Hold", VIN);
                                    listBox1.Refresh();
                                }
                                else if (orderStatus == STATUS_PAID)
                                {
                                    UpdateStock("No", VIN);
                                }

                                Purchase();

                            }
                            else
                            {
                                MessageBox.Show("Please select Order Status!");
                            }

                        }
                        else
                        {
                            MessageBox.Show("Please choose if this is a new or existing customer!");
                        }
                    }
                    
                    else if (DealerPurchaseCheck.Checked == false || TradeCheck.Checked == false)
                    {
                        MessageBox.Show("Please select a vehicle from the vehicle list!");
                    }

                    RefreshOrderLine();
                }

                else
                {
                    MessageBox.Show("Please select a dealer from the dropdown menu!");
                }

            }
            catch
            {

            }



        }

        private void Purchase()
        {

            if (TradeCheck.Checked == true)
            {
                if (MakeBox.Text == string.Empty || MakeBox.Text == string.Empty
                    || ModelBox.Text == string.Empty
                    || KMBox.Text == string.Empty
                    || PriceBox.Text == string.Empty
                    || YearBox.Text == string.Empty
                    || VINBox.Text == string.Empty
                    || ColourBox.Text == string.Empty)
                {
                    MessageBox.Show("Please fill in all fields for vehicle");
                }
                else
                {
                    //assign values from the input for trade ins or purchased vehicles
                    make = MakeBox.Text;
                    model = ModelBox.Text;
                    totalKM = KMBox.Text;
                    tradeIn = Convert.ToInt32(PriceBox.Text);
                    year = YearBox.Text;
                    VIN = VINBox.Text;
                    colour = ColourBox.Text;
                    inStock = "Yes";


                    //queries for inserting a new car model and make NOTE: ignoring duplicates
                    string buyCarMake = string.Format("INSERT IGNORE INTO Make (Make) VALUES (\'{0}\') ON DUPLICATE KEY UPDATE Make = \'{0}\';", make);
                    string buyCarModel = string.Format("INSERT IGNORE INTO Model (Model) VALUES ( \'{0}\') ON DUPLICATE KEY UPDATE Model = \'{0}\';", model);
                    string insertNewVehicle;

                    if (listBox1.SelectedItem != null)
                    {
                        //add a new vehicle query
                        insertNewVehicle = string.Format("INSERT INTO Vehicle (VehicleID, `Year`, MakeID, ModelID, Colour, TotalKM, WholesalePrice, DealershipID, InStock)" +
                                " VALUES(\'{0}\', {1}, (SELECT MakeID FROM Make WHERE Make = \'{2}\'), (SELECT  ModelID From Model WHERE Model = \'{3}\'), \'{4}\', {5}, {6}, {7}, \'{8}\');", VIN, year, make, model, colour, totalKM, tradeIn, dealerID, inStock);

                        //execute list of commands
                        using (var myConn = new MySqlConnection(myConnection))
                        {
                            var myCommand = new MySqlCommand(buyCarMake, myConn);
                            myConn.Open();
                            myCommand.ExecuteNonQuery();
                            myConn.Close();
                            myCommand = new MySqlCommand(buyCarModel, myConn);
                            myConn.Open();
                            myCommand.ExecuteNonQuery();
                            myConn.Close();
                            myCommand = new MySqlCommand(insertNewVehicle, myConn);
                            myConn.Open();
                            myCommand.ExecuteNonQuery();
                            myConn.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please select a vehicle!");
                    }
                }

                
                
            }
            //add order 
            AddOrder(firstName, lastName, phoneNumber);

            //set status to paid 
            orderStatus = STATUS_PAID;

            //fill orderline
            FillOrder(orderID, VIN, dealerID, orderStatus, tradeIn);//add trade in
            RefreshOrderLine();

        }

        /// <summary>
        /// This function simply handles the event for clicking on the existing customer option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadioExist_CheckedChanged(object sender, EventArgs e)
        {
            CustomersCombo.Visible = true;
            NewCustGroup.Visible = false;
        }

        /// <summary>
        /// This function simply handles the event for clicking on a new customer option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadioNew_CheckedChanged(object sender, EventArgs e)
        {
            CustomersCombo.Visible = false;
            NewCustGroup.Visible = true;

        }

        /// <summary>
        /// This function fills in the initial customer drop down menu
        /// </summary>
        private void FillCustomerBox()
        {
            totalCustomers = 0;

            //set up sql connection
            using (var myConn = new MySqlConnection(myConnection))
            {
                //query for getting all customers
                var myCommand = new MySqlCommand(@"SELECT * FROM Customer;", myConn);
 
                var myAdapter = new MySqlDataAdapter
                {
                    SelectCommand = myCommand
                };

                //store results in adaptor and data set
                var newDS = new DataSet();
                myAdapter.Fill(newDS, "TAWally.Customer");

                //add each customer to customer table
                foreach (DataRow dRow in newDS.Tables["TAWally.Customer"].Rows)
                {
                    var customerString = String.Format("{0, -10} {1, -10} {2, -15} ", dRow["FirstName"], dRow["lastName"], dRow["PhoneNumber"]);
                    CustomersCombo.Items.Add(customerString);
                    totalCustomers++;
                    customerID = totalCustomers;
                }

                //update total customer count
                totalCustomers++;

            }
        }

        /// <summary>
        /// This customer fills the orderline and calls the order summary function to display it
        /// </summary>
        /// <param name="orderID"></param>
        /// <param name="vehicleID"></param>
        /// <param name="dealerID"></param>
        /// <param name="statusID"></param>
        private void FillOrder(int orderID, string vehicleID, int dealerID, int statusID, int tradeIn)
        {
            if (DealerPurchaseCheck.Checked == true)
            {
                sPrice = Convert.ToDouble(PriceBox.Text);
                sPrice *= -1;
            }
            else
            {
                sPrice = wPrice * 1.4;
            }
            
            
            //query for adding info to orderline
            string addOrderLine = string.Format("INSERT INTO Orderline (OrderID, VehicleID, DealershipID, StatusID, sPrice, tradeIn) VALUES(\'{0}\', \'{1}\', \'{2}\', \'{3}\', {4}, {5});", orderID, vehicleID, dealerID, statusID, sPrice, tradeIn);

            //execute query
            using (var myConn = new MySqlConnection(myConnection))
            {
                var myCommand = new MySqlCommand(addOrderLine, myConn);
                myConn.Open();
                myCommand.ExecuteNonQuery();
                myConn.Close();

            }

            //finalize order summary (new window/form)
            GetOrderSummary();
        }

        /// <summary>
        /// This function adds a new customer to the customer database
        /// </summary>
        /// <param name="fName"></param>
        /// <param name="lName"></param>
        /// <param name="phoneNum"></param>
        private void AddCustomer(string fName, string lName, string phoneNum)
        {
            //add customer query
            string addCustomer = string.Format("INSERT INTO Customer (CustomerID, FirstName, LastName, PhoneNumber) VALUES(\'{0}\', \'{1}\', \'{2}\', \'{3}\');", totalCustomers + 1, fName, lName, phoneNum);
            
            //execute query
            using (var myConn = new MySqlConnection(myConnection))
            {
                var myCommand = new MySqlCommand(addCustomer, myConn);
                myConn.Open();
                myCommand.ExecuteNonQuery();
                myConn.Close();
                
            }
        }

        /// <summary>
        /// This function adds the order to the order database
        /// </summary>
        /// <param name="fName"></param>
        /// <param name="lName"></param>
        /// <param name="phoneNum"></param>
        private void AddOrder(string fName, string lName, string phoneNum)
        {
            //get customer info from database
            string customerInfoQuery = string.Format("SELECT CustomerID FROM Customer WHERE FirstName = \"{0}\" AND LastName = \'{1}\' AND PhoneNumber = \'{2}\';", fName, lName, phoneNum);

            //Query the databse for customer information 
            using (var myConn = new MySqlConnection(myConnection))
            {

                var myCommand = new MySqlCommand(customerInfoQuery, myConn);
                var myAdapter = new MySqlDataAdapter
                {
                    SelectCommand = myCommand
                };

                var newDS = new DataSet();
                myAdapter.Fill(newDS);

                

                customerID = Convert.ToInt32(newDS.Tables[0].Rows[0][0]);//customerID

                //add customer to order with todays date
                string addOrderQuery = string.Format("INSERT INTO `Order` (OrderDate, CustomerID) VALUES(\'{0}/{1}/{2}\', {3});", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, customerID);

                myCommand = new MySqlCommand(addOrderQuery, myConn);
                myConn.Open();
                myCommand.ExecuteNonQuery();
                myConn.Close();
                totalCustomers++;

                //get orderID
                orderID = GetOrderID(customerID);
            }
        }

        //This function gets the order ID from the customer ID through a query
        private int GetOrderID(int customerID)
        {
            int orderID;
            using (var myConn = new MySqlConnection(myConnection))
            {
                string getOrderIDQuery = string.Format("SELECT * FROM `Order` WHERE CustomerID = {0} ORDER BY OrderID DESC LIMIT 1 ;", customerID); //get most recent order
                var myCommand = new MySqlCommand(getOrderIDQuery, myConn);
 
                var myAdapter = new MySqlDataAdapter
                {
                    SelectCommand = myCommand
                };

                //set up adaptor to use a table so rows from query can be accessed
                DataSet newDS = new DataSet();
                DataTable newDT = new DataTable();

                myAdapter.Fill(newDS, "TAWally.Order");
                newDT = newDS.Tables["TAWally.Order"];

                return orderID = Convert.ToInt32(newDT.Rows[0][0]);//orderID
            }
        }

        /// <summary>
        /// This function translates the order indexes in the listbox to contants
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OrderStatusSelect(object sender, EventArgs e)
        {
            if (StatusCombo.SelectedIndex == 0)
            {
                orderStatus = STATUS_PAID;
            }
            else if (StatusCombo.SelectedIndex == 1)
            {
                orderStatus = STATUS_CANCEL;
            }
            else if (StatusCombo.SelectedIndex == 2)
            {
                orderStatus = STATUS_RFND;
            }
            else if (StatusCombo.SelectedIndex == 3)
            {
                orderStatus = STATUS_HOLD;
            }
        }

        /// <summary>
        /// This function gets the final order summary
        /// </summary>
        private void GetOrderSummary()
        {
            using (var myConn = new MySqlConnection(myConnection))
            {
                //query to select values that correspond to the current orderline
                var myCommand = new MySqlCommand(@"SELECT C.FirstName, C.LastName, O.OrderDate, O.OrderID, M.Make, Mo.Model, V.Year, V.TotalKM, V.WholesalePrice, OS.Status, V.Colour, V.VehicleID, C.CustomerID, D.DealerName" +
                         " FROM Dealership D, Vehicle V, Make M, Model Mo, `Order` O, Orderline OL, Customer C, OrderStatus OS" +
                         " WHERE V.MakeID = M.MakeID AND V.ModelID = Mo.ModelID AND C.CustomerID = O.CustomerID AND O.OrderID = OL.OrderID AND V.VehicleID = OL.VehicleID AND OS.StatusID = OL.StatusID AND D.DealershipID = OL.DealershipID ORDER BY OrderID DESC;", myConn);

                var myAdapter = new MySqlDataAdapter
                {
                    SelectCommand = myCommand
                };

                //use a dataset to access specific parts of the query result
                var newDS = new DataSet();
                myAdapter.Fill(newDS, "TAWally.OrderLine");
                double finalPrice;

                string prevOrderStringColumns = string.Format("{0, -10} {1, -10} {2, -10} {3, -10} {4, -10} {5, -10} {6, -3} {7, -10} {8, -10} {9, -10}", "First", "Last", "Make", "Model", "Colour", "Date", "OrderID", "VehicleID", "Status", "Price");
                DataTable orderSum = newDS.Tables["TAWally.Orderline"];

                //assign rows and columns to values in order to pass them to the summary form
                string customer = orderSum.Rows[0][0] + " " + orderSum.Rows[0][1];
                string date = orderSum.Rows[0][2].ToString();
                string orderID = orderSum.Rows[0][3].ToString();
                string make = orderSum.Rows[0][4].ToString();
                string model = orderSum.Rows[0][5].ToString();
                string year = orderSum.Rows[0][6].ToString();
                string totalKM = orderSum.Rows[0][7].ToString();
                int wPrice = Convert.ToInt32(orderSum.Rows[0][8]);
                string status = orderSum.Rows[0][9].ToString();
                string colour = orderSum.Rows[0][10].ToString();
                string VIN = orderSum.Rows[0][11].ToString();
                string location = orderSum.Rows[0][13].ToString();

                if (DealerPurchaseCheck.Checked == true)
                {
                    finalPrice = wPrice;
                }
                else
                {
                    finalPrice = sPrice; //edit final price to add  profit
                }
               
                
                
                

                //open new form and add values above
                PurchaseSummary summary = new PurchaseSummary(location, date, customer, orderID, status, make, model, totalKM, colour, wPrice, finalPrice, tradeIn, VIN, year);
                summary.Show();

            }
        }

        /// <summary>
        /// This function refreshes the order line window 
        /// </summary>
        private void RefreshOrderLine()
        {


            //access sql database
            using (var myConn = new MySqlConnection(myConnection))
            {
               //much like above, this query will select any and all unique orderlines
               var myCommand = new MySqlCommand(@"SELECT C.FirstName, C.LastName, O.OrderDate, O.OrderID, M.Make, Mo.Model, V.Year, V.TotalKM, V.WholesalePrice, OL.sPrice, OS.Status, V.Colour, V.VehicleID, C.CustomerID, D.DealerName, OL.TradeIn" +
                        " FROM Vehicle V, Make M, Model Mo, `Order` O, Orderline OL, Customer C, OrderStatus OS, Dealership D" +
                        " WHERE V.MakeID = M.MakeID AND V.ModelID = Mo.ModelID AND C.CustomerID = O.CustomerID AND O.OrderID = OL.OrderID AND V.VehicleID = OL.VehicleID AND OS.StatusID = OL.StatusID AND D.DealershipID = OL.DealershipID;", myConn);

                var myAdapter = new MySqlDataAdapter
                {
                    SelectCommand = myCommand
                };

                //add a dataset to store query result
                var newDS = new DataSet();
                myAdapter.Fill(newDS);

                dataGridView1.ReadOnly = true;
                dataGridView1.DataSource = newDS.Tables[0];

                
                
            }
        }

       
        /// <summary>
        /// This function handles the check event for dealer purchases and hides and 
        /// displays relevant information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DealerPurchaseChecked(object sender, EventArgs e)
        {
            if (DealerPurchaseCheck.Checked == true || TradeCheck.Checked == true)
            {
                MakeBox.Enabled = true;
                ModelBox.Enabled = true;
                KMBox.Enabled = true;
                PriceBox.Enabled = true;
                YearBox.Enabled = true;
                StockBox.Text = "Yes";
                VINBox.Enabled = true;
                ColourBox.Enabled = true;
            }
        }

        /// <summary>
        /// This function simply updates the stock to a new value in the databse
        /// </summary>
        /// <param name="stockType"></param>
        private void UpdateStock(string stockType, string VIN)
        {
            //query the database to update stock
            string updateStock = string.Format("UPDATE Vehicle" +
                " SET InStock = \'{0}\'" + " " +
                " WHERE VehicleID = \'{1}\';", stockType, VIN);

            //execute command
            using (var myConn = new MySqlConnection(myConnection))
            {
                var myCommand = new MySqlCommand(updateStock, myConn);
                myConn.Open();
                myCommand.ExecuteNonQuery();
                myConn.Close();
            }
        }

        /// <summary>
        /// This function will assign the purchased vehicle to be placed in the database for vehicles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Dealer_Purchase_Button(object sender, EventArgs e)
        {
            //check to make sure that a dealer location is selected
            if (DealerList.SelectedItem == null)
            {
                MessageBox.Show("Please select which dealership this car will be purchased to");
            }
            else
            {
                if (MakeBox.Text == string.Empty || MakeBox.Text == string.Empty
                        || ModelBox.Text == string.Empty
                        || KMBox.Text == string.Empty
                        || PriceBox.Text == string.Empty
                        || YearBox.Text == string.Empty
                        || VINBox.Text == string.Empty
                        || ColourBox.Text == string.Empty)
                {
                    MessageBox.Show("Please fill in all fields for vehicle");
                }
                else
                {
                    //assign values from the input for trade ins or purchased vehicles
                    make = MakeBox.Text;
                    model = ModelBox.Text;
                    totalKM = KMBox.Text;
                    year = YearBox.Text;
                    VIN = VINBox.Text;
                    colour = ColourBox.Text;
                    inStock = "Yes";
                    wPrice = Convert.ToInt32(PriceBox.Text);
                    tradeIn = 0;

                    //queries for inserting a new car model and make NOTE: ignoring duplicates
                    string buyCarMake = string.Format("INSERT IGNORE INTO Make (Make) VALUES (\'{0}\') ON DUPLICATE KEY UPDATE Make = \'{0}\';", make);
                    string buyCarModel = string.Format("INSERT IGNORE INTO Model (Model) VALUES ( \'{0}\') ON DUPLICATE KEY UPDATE Model = \'{0}\';", model);
                    string insertNewVehicle;

                    //add a new vehicle query
                    insertNewVehicle = string.Format("INSERT INTO Vehicle (VehicleID, `Year`, MakeID, ModelID, Colour, TotalKM, WholesalePrice, DealershipID, InStock)" +
                            " VALUES(\'{0}\', {1}, (SELECT MakeID FROM Make WHERE Make = \'{2}\'), (SELECT  ModelID From Model WHERE Model = \'{3}\'), \'{4}\', {5}, {6}, {7}, \'{8}\');", VIN, year, make, model, colour, totalKM, wPrice, dealerID,  inStock);

                    //execute list of commands
                    using (var myConn = new MySqlConnection(myConnection))
                    {
                        var myCommand = new MySqlCommand(buyCarMake, myConn);
                        myConn.Open();
                        myCommand.ExecuteNonQuery();
                        myConn.Close();
                        myCommand = new MySqlCommand(buyCarModel, myConn);
                        myConn.Open();
                        myCommand.ExecuteNonQuery();
                        myConn.Close();
                        myCommand = new MySqlCommand(insertNewVehicle, myConn);
                        myConn.Open();
                        myCommand.ExecuteNonQuery();
                        myConn.Close();
                    }

                    //add order 
                    AddOrder("Wally's World of Wheels", " ", "519-555-0000");

                    orderStatus = STATUS_PAID;

                    //fill orderline
                    FillOrder(orderID, VIN, dealerID, orderStatus, tradeIn);
                    RefreshOrderLine();
                }
            }

            
        }
        /// <summary>
        /// THis function sel;ects the info from the previous orders and loads them into a new window 
        /// that displays a purchase summary for that order
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PurchaseSummaryButton_Click(object sender, EventArgs e)
        {
            //fill in values from the selected row
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                string first = row.Cells[0].Value.ToString();
                string last = row.Cells[1].Value.ToString();
                string date = row.Cells[2].Value.ToString();
                string orderID = row.Cells[3].Value.ToString();
                string make = row.Cells[4].Value.ToString();
                string model = row.Cells[5].Value.ToString();
                string year = row.Cells[6].Value.ToString();
                string KM = row.Cells[7].Value.ToString();
                int wPrice = Convert.ToInt32(row.Cells[8].Value);
                string status = row.Cells[10].Value.ToString();
                string colour = row.Cells[11].Value.ToString();
                string VIN = row.Cells[12].Value.ToString();
                string customerID = row.Cells[13].Value.ToString();
                string dealerName = row.Cells[14].Value.ToString();
                int tradeIn = Convert.ToInt32(row.Cells[15].Value);

                string customerName = first + " " + last;
                double finalPrice;

                if (first == "Wally's World of Wheels")
                {
                    finalPrice = wPrice;
                    finalPrice *= -1;
                }
                else
                {
                    finalPrice = wPrice * 1.4;
                }
                

                PurchaseSummary newPOS = new PurchaseSummary(dealerName, date, customerName, orderID, status, make, model, KM, colour, wPrice, finalPrice, tradeIn, VIN, year);
                newPOS.ShowDialog();

            }


        }



        private void dataGridView1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows != null)
            {
                PurchaseSummaryButton.Enabled = true;
            }
        }

        private void DealerPurchaseCheck_Click(object sender, EventArgs e)
        {
            //check if dealer purchase box is checked
            if (DealerPurchaseCheck.Checked == true)
            {
                listBox1.Enabled = false;
                TradeCheck.Enabled = false;
                StatusCombo.Enabled = false;
                DealerPurchaseButton.Enabled = true;

            }
            else
            {
                listBox1.Enabled = true;
                TradeCheck.Enabled = true;
                StatusCombo.Enabled = true;
                DealerPurchaseButton.Enabled = false;
            }
        }
    }
}
