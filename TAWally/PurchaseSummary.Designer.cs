﻿namespace TAWally
{
    partial class PurchaseSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DateLbl = new System.Windows.Forms.Label();
            this.SubLbl = new System.Windows.Forms.Label();
            this.OrderLbl = new System.Windows.Forms.Label();
            this.HSTLbl = new System.Windows.Forms.Label();
            this.SummaryLbl = new System.Windows.Forms.Label();
            this.TradeLbl = new System.Windows.Forms.Label();
            this.PriceLbl = new System.Windows.Forms.Label();
            this.TotalLbl = new System.Windows.Forms.Label();
            this.CarLbl = new System.Windows.Forms.Label();
            this.CustLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // DateLbl
            // 
            this.DateLbl.AutoSize = true;
            this.DateLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateLbl.Location = new System.Drawing.Point(34, 66);
            this.DateLbl.Name = "DateLbl";
            this.DateLbl.Size = new System.Drawing.Size(39, 18);
            this.DateLbl.TabIndex = 1;
            this.DateLbl.Text = "Date";
            this.DateLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SubLbl
            // 
            this.SubLbl.AutoSize = true;
            this.SubLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubLbl.Location = new System.Drawing.Point(34, 330);
            this.SubLbl.Name = "SubLbl";
            this.SubLbl.Size = new System.Drawing.Size(34, 18);
            this.SubLbl.TabIndex = 10;
            this.SubLbl.Text = "Sub";
            this.SubLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // OrderLbl
            // 
            this.OrderLbl.AutoSize = true;
            this.OrderLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrderLbl.Location = new System.Drawing.Point(34, 150);
            this.OrderLbl.Name = "OrderLbl";
            this.OrderLbl.Size = new System.Drawing.Size(46, 18);
            this.OrderLbl.TabIndex = 2;
            this.OrderLbl.Text = "Order";
            this.OrderLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // HSTLbl
            // 
            this.HSTLbl.AutoSize = true;
            this.HSTLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HSTLbl.Location = new System.Drawing.Point(34, 367);
            this.HSTLbl.Name = "HSTLbl";
            this.HSTLbl.Size = new System.Drawing.Size(38, 18);
            this.HSTLbl.TabIndex = 9;
            this.HSTLbl.Text = "HST";
            this.HSTLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SummaryLbl
            // 
            this.SummaryLbl.AutoSize = true;
            this.SummaryLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SummaryLbl.Location = new System.Drawing.Point(34, 26);
            this.SummaryLbl.Name = "SummaryLbl";
            this.SummaryLbl.Size = new System.Drawing.Size(37, 18);
            this.SummaryLbl.TabIndex = 0;
            this.SummaryLbl.Text = "Intro";
            this.SummaryLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // TradeLbl
            // 
            this.TradeLbl.AutoSize = true;
            this.TradeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TradeLbl.Location = new System.Drawing.Point(34, 260);
            this.TradeLbl.Name = "TradeLbl";
            this.TradeLbl.Size = new System.Drawing.Size(46, 18);
            this.TradeLbl.TabIndex = 8;
            this.TradeLbl.Text = "Trade";
            this.TradeLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PriceLbl
            // 
            this.PriceLbl.AutoSize = true;
            this.PriceLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PriceLbl.Location = new System.Drawing.Point(34, 222);
            this.PriceLbl.Name = "PriceLbl";
            this.PriceLbl.Size = new System.Drawing.Size(42, 18);
            this.PriceLbl.TabIndex = 5;
            this.PriceLbl.Text = "Price";
            this.PriceLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // TotalLbl
            // 
            this.TotalLbl.AutoSize = true;
            this.TotalLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalLbl.Location = new System.Drawing.Point(34, 402);
            this.TotalLbl.Name = "TotalLbl";
            this.TotalLbl.Size = new System.Drawing.Size(41, 18);
            this.TotalLbl.TabIndex = 11;
            this.TotalLbl.Text = "Total";
            this.TotalLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CarLbl
            // 
            this.CarLbl.AutoSize = true;
            this.CarLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CarLbl.Location = new System.Drawing.Point(34, 189);
            this.CarLbl.Name = "CarLbl";
            this.CarLbl.Size = new System.Drawing.Size(32, 18);
            this.CarLbl.TabIndex = 6;
            this.CarLbl.Text = "Car";
            this.CarLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CustLbl
            // 
            this.CustLbl.AutoSize = true;
            this.CustLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CustLbl.Location = new System.Drawing.Point(34, 110);
            this.CustLbl.Name = "CustLbl";
            this.CustLbl.Size = new System.Drawing.Size(39, 18);
            this.CustLbl.TabIndex = 4;
            this.CustLbl.Text = "Cust";
            this.CustLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PurchaseSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TotalLbl);
            this.Controls.Add(this.SubLbl);
            this.Controls.Add(this.HSTLbl);
            this.Controls.Add(this.TradeLbl);
            this.Controls.Add(this.CarLbl);
            this.Controls.Add(this.PriceLbl);
            this.Controls.Add(this.CustLbl);
            this.Controls.Add(this.OrderLbl);
            this.Controls.Add(this.DateLbl);
            this.Controls.Add(this.SummaryLbl);
            this.Name = "PurchaseSummary";
            this.Text = "PurchaseSummary";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DateLbl;
        private System.Windows.Forms.Label SubLbl;
        private System.Windows.Forms.Label OrderLbl;
        private System.Windows.Forms.Label HSTLbl;
        private System.Windows.Forms.Label SummaryLbl;
        private System.Windows.Forms.Label TradeLbl;
        private System.Windows.Forms.Label PriceLbl;
        private System.Windows.Forms.Label TotalLbl;
        private System.Windows.Forms.Label CarLbl;
        private System.Windows.Forms.Label CustLbl;
    }
}