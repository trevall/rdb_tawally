﻿namespace TAWally
{
    partial class POSForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PurchaseButton = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.DealerList = new System.Windows.Forms.ComboBox();
            this.MakeBox = new System.Windows.Forms.TextBox();
            this.PriceBox = new System.Windows.Forms.TextBox();
            this.ColourBox = new System.Windows.Forms.TextBox();
            this.YearBox = new System.Windows.Forms.TextBox();
            this.ModelBox = new System.Windows.Forms.TextBox();
            this.MakeLbl = new System.Windows.Forms.Label();
            this.YearLbl = new System.Windows.Forms.Label();
            this.ModelLbl = new System.Windows.Forms.Label();
            this.PriceLbl = new System.Windows.Forms.Label();
            this.ColourLbl = new System.Windows.Forms.Label();
            this.StockLbl = new System.Windows.Forms.Label();
            this.StockBox = new System.Windows.Forms.TextBox();
            this.KMBox = new System.Windows.Forms.TextBox();
            this.KMLbl = new System.Windows.Forms.Label();
            this.VINBox = new System.Windows.Forms.TextBox();
            this.VINLbl = new System.Windows.Forms.Label();
            this.TradeCheck = new System.Windows.Forms.CheckBox();
            this.CustomersCombo = new System.Windows.Forms.ComboBox();
            this.RadioExist = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RadioNew = new System.Windows.Forms.RadioButton();
            this.LastLbl = new System.Windows.Forms.Label();
            this.PhonrLbl = new System.Windows.Forms.Label();
            this.FirstLbl = new System.Windows.Forms.Label();
            this.LastBox = new System.Windows.Forms.TextBox();
            this.PhoneBox = new System.Windows.Forms.TextBox();
            this.FirstBox = new System.Windows.Forms.TextBox();
            this.NewCustGroup = new System.Windows.Forms.GroupBox();
            this.StatusCombo = new System.Windows.Forms.ComboBox();
            this.DealerPurchaseCheck = new System.Windows.Forms.CheckBox();
            this.DealerPurchaseButton = new System.Windows.Forms.Button();
            this.PurchaseSummaryButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.NewCustGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // PurchaseButton
            // 
            this.PurchaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PurchaseButton.Location = new System.Drawing.Point(55, 662);
            this.PurchaseButton.Name = "PurchaseButton";
            this.PurchaseButton.Size = new System.Drawing.Size(240, 32);
            this.PurchaseButton.TabIndex = 4;
            this.PurchaseButton.Text = "Purchase";
            this.PurchaseButton.UseVisualStyleBackColor = true;
            this.PurchaseButton.Click += new System.EventHandler(this.Purchase_Click);
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(562, 79);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(902, 292);
            this.listBox1.TabIndex = 6;
            this.listBox1.Click += new System.EventHandler(this.POSForm_Click);
            // 
            // DealerList
            // 
            this.DealerList.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DealerList.FormattingEnabled = true;
            this.DealerList.Location = new System.Drawing.Point(129, 79);
            this.DealerList.Name = "DealerList";
            this.DealerList.Size = new System.Drawing.Size(287, 26);
            this.DealerList.TabIndex = 9;
            this.DealerList.Text = " - - Select Dealer - -";
            this.DealerList.SelectionChangeCommitted += new System.EventHandler(this.DealerList_Selection);
            // 
            // MakeBox
            // 
            this.MakeBox.Enabled = false;
            this.MakeBox.Location = new System.Drawing.Point(129, 175);
            this.MakeBox.Name = "MakeBox";
            this.MakeBox.Size = new System.Drawing.Size(100, 20);
            this.MakeBox.TabIndex = 10;
            // 
            // PriceBox
            // 
            this.PriceBox.Enabled = false;
            this.PriceBox.Location = new System.Drawing.Point(129, 375);
            this.PriceBox.Name = "PriceBox";
            this.PriceBox.Size = new System.Drawing.Size(100, 20);
            this.PriceBox.TabIndex = 11;
            // 
            // ColourBox
            // 
            this.ColourBox.Enabled = false;
            this.ColourBox.Location = new System.Drawing.Point(129, 292);
            this.ColourBox.Name = "ColourBox";
            this.ColourBox.Size = new System.Drawing.Size(100, 20);
            this.ColourBox.TabIndex = 12;
            // 
            // YearBox
            // 
            this.YearBox.Enabled = false;
            this.YearBox.Location = new System.Drawing.Point(129, 248);
            this.YearBox.Name = "YearBox";
            this.YearBox.Size = new System.Drawing.Size(100, 20);
            this.YearBox.TabIndex = 13;
            // 
            // ModelBox
            // 
            this.ModelBox.Enabled = false;
            this.ModelBox.Location = new System.Drawing.Point(129, 213);
            this.ModelBox.Name = "ModelBox";
            this.ModelBox.Size = new System.Drawing.Size(100, 20);
            this.ModelBox.TabIndex = 14;
            // 
            // MakeLbl
            // 
            this.MakeLbl.AutoSize = true;
            this.MakeLbl.Location = new System.Drawing.Point(55, 175);
            this.MakeLbl.Name = "MakeLbl";
            this.MakeLbl.Size = new System.Drawing.Size(40, 13);
            this.MakeLbl.TabIndex = 15;
            this.MakeLbl.Text = "Make: ";
            // 
            // YearLbl
            // 
            this.YearLbl.AutoSize = true;
            this.YearLbl.Location = new System.Drawing.Point(55, 251);
            this.YearLbl.Name = "YearLbl";
            this.YearLbl.Size = new System.Drawing.Size(35, 13);
            this.YearLbl.TabIndex = 16;
            this.YearLbl.Text = "Year: ";
            // 
            // ModelLbl
            // 
            this.ModelLbl.AutoSize = true;
            this.ModelLbl.Location = new System.Drawing.Point(55, 213);
            this.ModelLbl.Name = "ModelLbl";
            this.ModelLbl.Size = new System.Drawing.Size(42, 13);
            this.ModelLbl.TabIndex = 17;
            this.ModelLbl.Text = "Model: ";
            // 
            // PriceLbl
            // 
            this.PriceLbl.AutoSize = true;
            this.PriceLbl.Location = new System.Drawing.Point(55, 375);
            this.PriceLbl.Name = "PriceLbl";
            this.PriceLbl.Size = new System.Drawing.Size(34, 13);
            this.PriceLbl.TabIndex = 18;
            this.PriceLbl.Text = "Price:";
            // 
            // ColourLbl
            // 
            this.ColourLbl.AutoSize = true;
            this.ColourLbl.Location = new System.Drawing.Point(55, 292);
            this.ColourLbl.Name = "ColourLbl";
            this.ColourLbl.Size = new System.Drawing.Size(40, 13);
            this.ColourLbl.TabIndex = 19;
            this.ColourLbl.Text = "Colour:";
            // 
            // StockLbl
            // 
            this.StockLbl.AutoSize = true;
            this.StockLbl.Location = new System.Drawing.Point(56, 411);
            this.StockLbl.Name = "StockLbl";
            this.StockLbl.Size = new System.Drawing.Size(50, 13);
            this.StockLbl.TabIndex = 20;
            this.StockLbl.Text = "In Stock:";
            // 
            // StockBox
            // 
            this.StockBox.Enabled = false;
            this.StockBox.Location = new System.Drawing.Point(129, 411);
            this.StockBox.Name = "StockBox";
            this.StockBox.Size = new System.Drawing.Size(100, 20);
            this.StockBox.TabIndex = 21;
            // 
            // KMBox
            // 
            this.KMBox.Enabled = false;
            this.KMBox.Location = new System.Drawing.Point(129, 335);
            this.KMBox.Name = "KMBox";
            this.KMBox.Size = new System.Drawing.Size(100, 20);
            this.KMBox.TabIndex = 22;
            // 
            // KMLbl
            // 
            this.KMLbl.AutoSize = true;
            this.KMLbl.Location = new System.Drawing.Point(52, 335);
            this.KMLbl.Name = "KMLbl";
            this.KMLbl.Size = new System.Drawing.Size(58, 13);
            this.KMLbl.TabIndex = 23;
            this.KMLbl.Text = "Total KMs:";
            // 
            // VINBox
            // 
            this.VINBox.Enabled = false;
            this.VINBox.Location = new System.Drawing.Point(129, 450);
            this.VINBox.Name = "VINBox";
            this.VINBox.Size = new System.Drawing.Size(100, 20);
            this.VINBox.TabIndex = 24;
            // 
            // VINLbl
            // 
            this.VINLbl.AutoSize = true;
            this.VINLbl.Location = new System.Drawing.Point(56, 453);
            this.VINLbl.Name = "VINLbl";
            this.VINLbl.Size = new System.Drawing.Size(31, 13);
            this.VINLbl.TabIndex = 25;
            this.VINLbl.Text = "VIN: ";
            // 
            // TradeCheck
            // 
            this.TradeCheck.AutoSize = true;
            this.TradeCheck.Location = new System.Drawing.Point(58, 526);
            this.TradeCheck.Name = "TradeCheck";
            this.TradeCheck.Size = new System.Drawing.Size(72, 17);
            this.TradeCheck.TabIndex = 26;
            this.TradeCheck.Text = "Trade In?";
            this.TradeCheck.UseVisualStyleBackColor = true;
            this.TradeCheck.CheckedChanged += new System.EventHandler(this.DealerPurchaseChecked);
            // 
            // CustomersCombo
            // 
            this.CustomersCombo.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CustomersCombo.FormattingEnabled = true;
            this.CustomersCombo.Location = new System.Drawing.Point(249, 175);
            this.CustomersCombo.Name = "CustomersCombo";
            this.CustomersCombo.Size = new System.Drawing.Size(296, 24);
            this.CustomersCombo.TabIndex = 28;
            this.CustomersCombo.Text = " -- Existing Customer --";
            // 
            // RadioExist
            // 
            this.RadioExist.AutoSize = true;
            this.RadioExist.Location = new System.Drawing.Point(6, 19);
            this.RadioExist.Name = "RadioExist";
            this.RadioExist.Size = new System.Drawing.Size(108, 17);
            this.RadioExist.TabIndex = 29;
            this.RadioExist.TabStop = true;
            this.RadioExist.Text = "Existing Customer";
            this.RadioExist.UseVisualStyleBackColor = true;
            this.RadioExist.CheckedChanged += new System.EventHandler(this.RadioExist_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RadioNew);
            this.groupBox1.Controls.Add(this.RadioExist);
            this.groupBox1.Location = new System.Drawing.Point(249, 213);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(121, 87);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            // 
            // RadioNew
            // 
            this.RadioNew.AutoSize = true;
            this.RadioNew.Location = new System.Drawing.Point(6, 54);
            this.RadioNew.Name = "RadioNew";
            this.RadioNew.Size = new System.Drawing.Size(94, 17);
            this.RadioNew.TabIndex = 30;
            this.RadioNew.TabStop = true;
            this.RadioNew.Text = "New Customer";
            this.RadioNew.UseVisualStyleBackColor = true;
            this.RadioNew.CheckedChanged += new System.EventHandler(this.RadioNew_CheckedChanged);
            // 
            // LastLbl
            // 
            this.LastLbl.AutoSize = true;
            this.LastLbl.Location = new System.Drawing.Point(17, 61);
            this.LastLbl.Name = "LastLbl";
            this.LastLbl.Size = new System.Drawing.Size(33, 13);
            this.LastLbl.TabIndex = 36;
            this.LastLbl.Text = "Last: ";
            // 
            // PhonrLbl
            // 
            this.PhonrLbl.AutoSize = true;
            this.PhonrLbl.Location = new System.Drawing.Point(17, 99);
            this.PhonrLbl.Name = "PhonrLbl";
            this.PhonrLbl.Size = new System.Drawing.Size(44, 13);
            this.PhonrLbl.TabIndex = 35;
            this.PhonrLbl.Text = "Phone: ";
            // 
            // FirstLbl
            // 
            this.FirstLbl.AutoSize = true;
            this.FirstLbl.Location = new System.Drawing.Point(17, 23);
            this.FirstLbl.Name = "FirstLbl";
            this.FirstLbl.Size = new System.Drawing.Size(29, 13);
            this.FirstLbl.TabIndex = 34;
            this.FirstLbl.Text = "First:";
            // 
            // LastBox
            // 
            this.LastBox.Location = new System.Drawing.Point(91, 61);
            this.LastBox.Name = "LastBox";
            this.LastBox.Size = new System.Drawing.Size(100, 20);
            this.LastBox.TabIndex = 33;
            // 
            // PhoneBox
            // 
            this.PhoneBox.Location = new System.Drawing.Point(91, 96);
            this.PhoneBox.Name = "PhoneBox";
            this.PhoneBox.Size = new System.Drawing.Size(100, 20);
            this.PhoneBox.TabIndex = 32;
            // 
            // FirstBox
            // 
            this.FirstBox.Location = new System.Drawing.Point(91, 23);
            this.FirstBox.Name = "FirstBox";
            this.FirstBox.Size = new System.Drawing.Size(100, 20);
            this.FirstBox.TabIndex = 31;
            // 
            // NewCustGroup
            // 
            this.NewCustGroup.Controls.Add(this.FirstLbl);
            this.NewCustGroup.Controls.Add(this.LastLbl);
            this.NewCustGroup.Controls.Add(this.FirstBox);
            this.NewCustGroup.Controls.Add(this.PhonrLbl);
            this.NewCustGroup.Controls.Add(this.PhoneBox);
            this.NewCustGroup.Controls.Add(this.LastBox);
            this.NewCustGroup.Location = new System.Drawing.Point(249, 306);
            this.NewCustGroup.Name = "NewCustGroup";
            this.NewCustGroup.Size = new System.Drawing.Size(278, 164);
            this.NewCustGroup.TabIndex = 37;
            this.NewCustGroup.TabStop = false;
            this.NewCustGroup.Text = "Enter New Customer Information";
            this.NewCustGroup.Visible = false;
            // 
            // StatusCombo
            // 
            this.StatusCombo.FormattingEnabled = true;
            this.StatusCombo.Location = new System.Drawing.Point(59, 614);
            this.StatusCombo.Name = "StatusCombo";
            this.StatusCombo.Size = new System.Drawing.Size(121, 21);
            this.StatusCombo.TabIndex = 39;
            this.StatusCombo.Text = "-- Order Status --";
            this.StatusCombo.SelectedIndexChanged += new System.EventHandler(this.OrderStatusSelect);
            // 
            // DealerPurchaseCheck
            // 
            this.DealerPurchaseCheck.AutoSize = true;
            this.DealerPurchaseCheck.Location = new System.Drawing.Point(58, 567);
            this.DealerPurchaseCheck.Name = "DealerPurchaseCheck";
            this.DealerPurchaseCheck.Size = new System.Drawing.Size(111, 17);
            this.DealerPurchaseCheck.TabIndex = 40;
            this.DealerPurchaseCheck.Text = "Dealer Purchase?";
            this.DealerPurchaseCheck.UseVisualStyleBackColor = true;
            this.DealerPurchaseCheck.CheckedChanged += new System.EventHandler(this.DealerPurchaseChecked);
            this.DealerPurchaseCheck.Click += new System.EventHandler(this.DealerPurchaseCheck_Click);
            // 
            // DealerPurchaseButton
            // 
            this.DealerPurchaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DealerPurchaseButton.Location = new System.Drawing.Point(316, 662);
            this.DealerPurchaseButton.Name = "DealerPurchaseButton";
            this.DealerPurchaseButton.Size = new System.Drawing.Size(240, 32);
            this.DealerPurchaseButton.TabIndex = 41;
            this.DealerPurchaseButton.Text = "Dealer Purchase";
            this.DealerPurchaseButton.UseVisualStyleBackColor = true;
            this.DealerPurchaseButton.Click += new System.EventHandler(this.Dealer_Purchase_Button);
            // 
            // PurchaseSummaryButton
            // 
            this.PurchaseSummaryButton.Enabled = false;
            this.PurchaseSummaryButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PurchaseSummaryButton.Location = new System.Drawing.Point(316, 511);
            this.PurchaseSummaryButton.Name = "PurchaseSummaryButton";
            this.PurchaseSummaryButton.Size = new System.Drawing.Size(240, 32);
            this.PurchaseSummaryButton.TabIndex = 42;
            this.PurchaseSummaryButton.Text = "Puchase Summary";
            this.PurchaseSummaryButton.UseVisualStyleBackColor = true;
            this.PurchaseSummaryButton.Click += new System.EventHandler(this.PurchaseSummaryButton_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(562, 411);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(902, 283);
            this.dataGridView1.TabIndex = 43;
            this.dataGridView1.Click += new System.EventHandler(this.dataGridView1_Click);
            // 
            // POSForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1507, 718);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.PurchaseSummaryButton);
            this.Controls.Add(this.DealerPurchaseButton);
            this.Controls.Add(this.DealerPurchaseCheck);
            this.Controls.Add(this.StatusCombo);
            this.Controls.Add(this.NewCustGroup);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CustomersCombo);
            this.Controls.Add(this.TradeCheck);
            this.Controls.Add(this.VINLbl);
            this.Controls.Add(this.VINBox);
            this.Controls.Add(this.KMLbl);
            this.Controls.Add(this.KMBox);
            this.Controls.Add(this.StockBox);
            this.Controls.Add(this.StockLbl);
            this.Controls.Add(this.ColourLbl);
            this.Controls.Add(this.PriceLbl);
            this.Controls.Add(this.ModelLbl);
            this.Controls.Add(this.YearLbl);
            this.Controls.Add(this.MakeLbl);
            this.Controls.Add(this.ModelBox);
            this.Controls.Add(this.YearBox);
            this.Controls.Add(this.ColourBox);
            this.Controls.Add(this.PriceBox);
            this.Controls.Add(this.MakeBox);
            this.Controls.Add(this.DealerList);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.PurchaseButton);
            this.Name = "POSForm";
            this.Text = "TAWally";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.NewCustGroup.ResumeLayout(false);
            this.NewCustGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button PurchaseButton;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ComboBox DealerList;
        private System.Windows.Forms.TextBox MakeBox;
        private System.Windows.Forms.TextBox PriceBox;
        private System.Windows.Forms.TextBox ColourBox;
        private System.Windows.Forms.TextBox YearBox;
        private System.Windows.Forms.TextBox ModelBox;
        private System.Windows.Forms.Label MakeLbl;
        private System.Windows.Forms.Label YearLbl;
        private System.Windows.Forms.Label ModelLbl;
        private System.Windows.Forms.Label PriceLbl;
        private System.Windows.Forms.Label ColourLbl;
        private System.Windows.Forms.Label StockLbl;
        private System.Windows.Forms.TextBox StockBox;
        private System.Windows.Forms.TextBox KMBox;
        private System.Windows.Forms.Label KMLbl;
        private System.Windows.Forms.TextBox VINBox;
        private System.Windows.Forms.Label VINLbl;
        private System.Windows.Forms.CheckBox TradeCheck;
        private System.Windows.Forms.ComboBox CustomersCombo;
        private System.Windows.Forms.RadioButton RadioExist;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton RadioNew;
        private System.Windows.Forms.Label LastLbl;
        private System.Windows.Forms.Label PhonrLbl;
        private System.Windows.Forms.Label FirstLbl;
        private System.Windows.Forms.TextBox LastBox;
        private System.Windows.Forms.TextBox PhoneBox;
        private System.Windows.Forms.TextBox FirstBox;
        private System.Windows.Forms.GroupBox NewCustGroup;
        private System.Windows.Forms.ComboBox StatusCombo;
        private System.Windows.Forms.CheckBox DealerPurchaseCheck;
        private System.Windows.Forms.Button DealerPurchaseButton;
        private System.Windows.Forms.Button PurchaseSummaryButton;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}

