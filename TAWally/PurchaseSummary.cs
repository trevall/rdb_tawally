﻿//
// FILE : PurchaseSumary.cs
// PROJECT : PROG2110 - Assignment #4
// PROGRAMMER : Trevor Allain
// FIRST VERSION : 2018-12-01
// DESCRIPTION : The source code for the logic of the POSForm. This program will act like a point of sale application
//               where users can purchase vehicles from a number of stores. Also, stores can purchase vehicles as well as
//               allow for the trade in of cars.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TAWally
{
    public partial class PurchaseSummary : Form
    {
        public PurchaseSummary(string location, string date, string customer, string orderID, string orderStatus, string make, string model, string totalKM, string colour, int wPrice, double finalPrice, int tradeIn, string VIN, string year)
        {
            InitializeComponent();

            

            //below is simply populating the text boxes for the order summary
            SummaryLbl.Text = string.Format("Thank you for choosing Wally's World of Wheels at {0} for your quality used vehicle!", location);
            DateLbl.Text = string.Format("Date: On {0}", date);
            CustLbl.Text = string.Format("Customer: {0}", customer);
            OrderLbl.Text = string.Format("Order ID: {0} - {1}", orderID, orderStatus);
            CarLbl.Text = string.Format("{0} {1} {2}, {3}\nVIN: {4} KMS: {5}", year, make, model, colour, VIN, totalKM);
            PriceLbl.Text = string.Format("Purchase Price: ${0}", finalPrice);
            TradeLbl.Text = string.Format("Trade In: ${0}", tradeIn);

            //if final price is greater than zero it means that it is NOT a dealer purchase (tax must be added)
            if (finalPrice > 0)
            {
                
                SubLbl.Text = string.Format("Subtotal = ${0}", finalPrice);
                HSTLbl.Text = string.Format("HST (13%) = ${0}", finalPrice * 0.13);
                finalPrice = (finalPrice * 1.13) - tradeIn;
                TotalLbl.Text = string.Format("Sale Total = ${0}", finalPrice);
            }
            else
            {
                SubLbl.Text = string.Format("Subtotal = ${0}", finalPrice);
                HSTLbl.Text = string.Format("HST (13%) = $0 -- (Included in dealer purchase)");
                TotalLbl.Text = string.Format("Sale Total = ${0}", finalPrice);
            }

            

            
        }
    }
}
